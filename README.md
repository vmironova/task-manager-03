# TASK-MANAGER

## DEVELOPER

**NAME**: Veronika Mironova

**E-MAIL**: vmironova@t1-consulting.ru

## SOFTWARE

**JAVA**: JDK 1.8

**OS**: Windows 10

## HARDWARE

**CPU**: i5

**RAM**: 16Gb

## APPLICATION RUN

```bash
java -jar ./task-manager.jar
```
